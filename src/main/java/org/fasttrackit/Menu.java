package org.fasttrackit;

public class Menu {

    private final String foodItem;
    private final int price;

    private final String calories;

    public Menu(String foodItem, int price, String calories) {
        this.foodItem = foodItem;
        this.price = price;
        this.calories = calories;
    }

    public String getCalories() {
        return calories;
    }

    public String getFoodItem() {
        return foodItem;
    }

    public int getPrice() {
        return price;
    }

    //    public String toString() {
//        String menu = "\n - " + name;
//        for (int i = 0; i < foodItems.size(); i++) {
//            menu += "\n " + (i + 1) + ". " + foodItems.get(i) + " " + foodPrice.get(i) + " " + foodCalories.get(i);
//        }
//        return menu;
//    }

}





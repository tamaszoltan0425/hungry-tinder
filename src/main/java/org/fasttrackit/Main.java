package org.fasttrackit;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static List<Restaurants> restaurants = new ArrayList<>();
    private static List<Cart> carts = new ArrayList<>();

    private static final String MESSAGEMENU = " \n What would you like to do next? " + "\n 1. Leave a review for this restaurant. " + "\n 2. Read the reviews for this restaurant." +"\n 3. Order food from this restaurant" + "\n 4. Back.";
    private static Scanner scanner;

    public static void main(String[] args) {

        scanner = new Scanner(System.in);

        welcomeMessage();

        buildRestaurants();

        int selection;
        int option3 = 2;


        do {
            showRestaurants();
            System.out.println("5. EXIT");

            System.out.print("Choose a restaurant: ");
            selection = scanner.nextInt();

            selection = getCorrectInt(selection);

            if (selection == 5) {
                System.out.println("Bye!!!!");

            } else {
                Restaurants restSelectedByUser = restaurants.get(selection - 1);
                System.out.println("\n You have selected " + restSelectedByUser.getName());

                System.out.println(MESSAGEMENU);
                System.out.print("\n Choose a number: ");

                int option1 = getOption1();

                switch (option1) {
                    case 1:
                        System.out.println("Leave a review: " + "\n");
                        System.out.print("Your name: ");
                        String custName = scanner.next();
                        System.out.print("Your thoughts about " + restSelectedByUser.getName() + ": ");
                        String custReview = scanner.next();
                        System.out.println("Thank you for your review!");
                        restSelectedByUser.addReview(custName, custReview);
                        break;
                    case 2:
                        System.out.println("Reviews: " + "\n" + restSelectedByUser.getReview());
                        break;
                    case 3:
                        int option2;

                        do {
                            System.out.println(restSelectedByUser.getMenu());
                            System.out.println("\nPlease enter a number:");
                            option2 = scanner.nextInt();

                            if (option2 == 4) {
                                System.out.println("Your cart contains: ");
                                int totalPrice = 0;
                                for (int i = 0; i < carts.size(); i++) {

                                    System.out.println((i + 1) + ". " + carts.get(i).getFood() + " " + carts.get(i).getPrice() + " RON");
                                    totalPrice += carts.get(i).getPrice();
                                }
                                System.out.println("Your total is: " + totalPrice + " RON");
                                if (restSelectedByUser.hasSpeedDelivery()) {
                                    System.out.println(restSelectedByUser.getName() + " offers express delivery for 10 RON extra. \n 1. Express or 2.traffic rhythm?");
                                    System.out.print("Choose delivery option: ");
                                    int deliveryOpt = scanner.nextInt();
                                    if (deliveryOpt == 1) {
                                        totalPrice += 10;
                                        System.out.println("Your new total: " + totalPrice);
                                    } else if (deliveryOpt == 2) {
                                        System.out.println("We' ll get there eventually");
                                    } else {
                                        System.out.println("Wrong number!");
                                    }
                                }

                                break;
                            } else {
                                String nameOfFoodSelected = restSelectedByUser.getFoodFromMenu(option2 - 1);
                                int priceOfFoodSelected = restSelectedByUser.getPriceFromMenu(option2 - 1);

                                System.out.println(nameOfFoodSelected + " added to the Cart!");
                                carts.add(new Cart(nameOfFoodSelected, priceOfFoodSelected));
                            }

                        } while (option2 < 4);

                        break;
                    case 4:
                    default:
                        break;
                }
                System.out.println("What's next: 1)exit or 2)main page?:");
                option3 = scanner.nextInt();
            }
        } while (option3 == 2);
    }

    private static int getCorrectInt(int selection) {
        while ((selection < 1) || (selection > 5)) {
            System.out.println("Wrong number, try again: ");
            selection = scanner.nextInt();
        }
        return selection;
    }

    private static int getOption1() {
        int option1;
        do {
            option1 = scanner.nextInt();
            if (option1 > restaurants.size()) {
                System.out.print("Wrong number, try again: ");
            }
        }
        while (option1 > restaurants.size());
        return option1;
    }

    private static void showRestaurants() {
        System.out.println("Choose a restaurant from the list below: " + "\n");

        for (int i = 0; i < restaurants.size(); i++) {

            System.out.println((i + 1) + ". " + restaurants.get(i).getName());
        }
    }

    private static void buildRestaurants() {

        Restaurants gluthen = new Restaurants("Gluthen", "Cluj", 3);

        Restaurants vegan = new Restaurants("Vegan Heaven", "Cluj", 5);

        Restaurants notvegan = new Restaurants("Meat Eater's Paradise", "Mamaia", 1);

        Restaurants sweet = new Restaurants("Cakes 'R Us", "Brasov", 4);

        buildDelivery(gluthen, vegan, notvegan, sweet);

        buildReviews(sweet, gluthen, vegan, notvegan);

        buildMenu(sweet, notvegan, vegan, gluthen);


        restaurants.add(gluthen);
        restaurants.add(vegan);
        restaurants.add(notvegan);
        restaurants.add(sweet);
    }

    private static void buildDelivery(Restaurants gluthen, Restaurants vegan, Restaurants notvegan, Restaurants sweet) {
        gluthen.addDeliverySpeed(false);
        vegan.addDeliverySpeed(true);
        notvegan.addDeliverySpeed(false);
        sweet.addDeliverySpeed(true);
    }

    private static void buildReviews(Restaurants sweet, Restaurants gluthen, Restaurants vegan, Restaurants
            notvegan) {
        sweet.addReview("Elizabeth", "Chocolate cake too chocolatey");
        sweet.addReview("Francis", "Really, Elizabeth?!");
        gluthen.addReview("Agnes", "Bread was fresh!");
        gluthen.addReview("Javier", "Best flour ever!");
        vegan.addReview("Karen", "OMG, like, so fresh!");
        vegan.addReview("Philip", "OMG, like, so ethical!");
        notvegan.addReview("Justin", "I ate the bone as well");
        notvegan.addReview("Rex", "woof woof woof");
    }

    private static void buildMenu(Restaurants sweet, Restaurants notvegan, Restaurants vegan, Restaurants gluthen) {
        sweet.addFood("Chocolate Cake", 11, "300 kcal");
        sweet.addFood("Chocolate Mousse", 10, "310 kcal");
        sweet.addFood("Chocolate Ice Cream", 9, "110 kcal");
        notvegan.addFood("Veal Stew", 30, "600 kcal");
        notvegan.addFood("Braised Duck Breast", 50, "470 kcal");
        notvegan.addFood("Bloody Rare T-Bone Steak", 150, "1000 kcal");
        vegan.addFood("Organic Carrot Stick", 25, "13 Kcal");
        vegan.addFood("Organic Celery Stick", 26, "11 Kcal");
        vegan.addFood("Organic Asparagus Stick", 28, "9 Kcal");
        gluthen.addFood("Sliced Bread on White Toast", 10, "150 kcal");
        gluthen.addFood("Fluffy Croissant in a Bagel", 8, "170 kcal");
        gluthen.addFood("Pumpkin Bread filled Banana Bread", 18, "220 kcal");
    }

    private static void welcomeMessage() {
        System.out.println("The Hungry People's Tinder. " + "\n");
        System.out.println("Find a match for your hunger!" + "\n");
    }
}


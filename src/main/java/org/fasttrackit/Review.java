package org.fasttrackit;

import java.util.Collections;
import java.util.List;

public class Review {

    private final String author;

    private final String review;

    public Review(String author, String review) {
        this.author = author;
        this.review = review;
    }

    public String getAuthor() {
        return author;
    }

    public String getReview() {
        return review;
    }
}

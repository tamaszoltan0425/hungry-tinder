package org.fasttrackit;

public class Cart {

    private final String food;

    private final int price;

    public Cart(String food, int total) {
        this.food = food;
        this.price = total;
    }

    public int getPrice() {
        return price;
    }

    public String getFood() {
        return food;
    }
}

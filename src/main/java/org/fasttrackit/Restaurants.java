package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

public class Restaurants {

    private final String name;

    private final String address;

    private final int rating;
    private final List<Menu> menus = new ArrayList<>();

    private final List<Review> reviews = new ArrayList<>();
    private boolean deliverySpeed = false;

    public Restaurants(String name, String address, int rating) {
        this.name = name;
        this.address = address;
        this.rating = rating;
    }

    public void addDeliverySpeed(boolean delivery) {
        this.deliverySpeed = delivery;
    }

    public boolean hasSpeedDelivery() {
        return deliverySpeed;
    }
    public String getName() {
        return name;
    }

    public int getRating() {
        return rating;
    }

    public String getAddress() {
        return address;
    }

    public String getMenu() {
        String menu = "\n - " + name;
        for (int i = 0; i < menus.size() ; i++) {
            menu += "\n " + (i + 1) + ". " + menus.get(i).getFoodItem() + " " + menus.get(i).getPrice() + " RON";
        }
        menu += "\n 4. Check out";
        return menu;
    }



    public void addFood(String item, int price, String kcal) {
        menus.add(new Menu(item, price, kcal));
    }
    public void addReview(String name, String review) {
        reviews.add(new Review(name, review));
    }

    public String getReview() {
        String allReviews = "All Reviews: "+ "\n";
        for (int i = 0; i < reviews.size(); i++) {
            allReviews += "\n" + reviews.get(i).getAuthor()+"\n"+reviews.get(i).getReview();
        } return allReviews;
    }

    public String getFoodFromMenu(int foodIndex) {
        return menus.get(foodIndex).getFoodItem();
    }
    public int getPriceFromMenu(int foodIndex) {
        return menus.get(foodIndex).getPrice();
    }


}
